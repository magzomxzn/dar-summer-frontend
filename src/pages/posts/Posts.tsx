import React, { useEffect } from 'react';

import './Posts.scss';
import { Post } from '../../types/interfaces';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { AppState } from '../../state/store';
import { fetchPosts } from '../../state/posts/actions';

type Props = {
    posts: Post[];
    fetchPosts: () => void;
}

const Posts: React.FunctionComponent<Props> = ({posts, fetchPosts}) => {

    useEffect(() => {
        fetchPosts();
    }, []);
    
    return (
        <div className="Posts">
            <div className="posts-list">
                {
                    posts.map(post => (
                        <article className="posts-item" key={post.id}>
                            <Link to={`/posts/${post.id}`}><h3>{post.title}</h3></Link>
                        </article>
                    ))
                }
            </div>
        </div>
    );
}

export default connect(
    (state: AppState) => ({
        posts: state.posts.items
    }),
    {
        fetchPosts
    }
)(Posts);
