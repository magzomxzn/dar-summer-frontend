import React, {useEffect, useState} from "react";
import { NavLink } from "react-router-dom";
import { Post } from "../../types/interfaces";
import "./PostItem.scss";
import { getPost } from "../../services/api";


export const PostItem = ({id}: {id: number}) => {

   const [ item, setItem ] = useState<Post | null>(null);
   
   useEffect( () => {
    getPost(id)
        .then((response) => {
            console.log(response);
            setItem(response)
        })
        .catch(err => console.log(err));
   }, [id]);


    return (
            <div className="PostItem">
                {   item ?
                        <article key={ item.id } className="PostItem-list">
                            <p>id: { item.id }</p>
                            <p>userId: { item.userId }</p>
                            <p>title: { item.title }</p>
                            <p>text: { item.body }</p>
                        </article>
                    : null
                }
                <NavLink to="/posts"><button className="PostItem-btn">Go back</button></NavLink>
            </div>

    );

};
