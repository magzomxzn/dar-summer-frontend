
import { call, put, takeLatest } from 'redux-saga/effects';
import { getPosts } from '../../services/api';
import { PostsActions } from './actions';

export function* fetchPosts() {
    try {
        const posts = yield call(getPosts);
        yield put({type: PostsActions.FETCH_POSTS_SUCCESS, payload: posts});
     } catch (e) {
        yield put({type: PostsActions.FETCH_POSTS_ERROR, payload: e.message});
     }
}


function* postsSaga() {
    yield takeLatest(PostsActions.FETCH_POSTS, fetchPosts);
}
  
export default postsSaga;