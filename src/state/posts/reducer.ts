import { PostsActionTypes } from './actions';
import { Post } from "../../types/interfaces";


export interface PostsState {
    items: Post[];
}

const initialState: PostsState  = {
    items: [],
}

export default function(state = initialState, 
    action: {type: PostsActionTypes, payload: any}) {

    switch(action.type) {
        case PostsActionTypes.SET_POSTS:
            return {
                ...state,
                items: action.payload,
            }
        default:
            return state;
    }    
}