import { PostsActionTypes } from './actions';
import { call, put, takeLatest } from 'redux-saga/effects';
import { getPosts } from './../../services/api';
import { Post } from '../../types/interfaces';

export function* fetchPosts() {
    try {
        const items = yield call(getPosts);
        yield put({type: PostsActionTypes.FETCH_POSTS_SUCCESS, payload: items})
    } catch (e) {
        yield put({type: PostsActionTypes.FETCH_POSTS_ERROR, payload: e.message})
    }
}

export function* setPosts(action: any) {
    yield put({type: PostsActionTypes.SET_POSTS, payload: action.payload})
}

function* postsSaga() {
    yield takeLatest(PostsActionTypes.FETCH_POSTS, fetchPosts);
    yield takeLatest(PostsActionTypes.FETCH_POSTS_SUCCESS, setPosts);
}

export default postsSaga;