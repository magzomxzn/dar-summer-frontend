import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import postsReducer, { PostsState } from './posts/reducer';
import postsSaga from './posts/saga';

const reducers = combineReducers({
    posts: postsReducer
});

const sagaMiddleware = createSagaMiddleware();

export interface AppState {
    posts: PostsState;
}

export default createStore(reducers, composeWithDevTools(
    applyMiddleware(sagaMiddleware),
));

sagaMiddleware.run(postsSaga);
